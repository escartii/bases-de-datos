## Apuntes Álvaro Escartí - 1º DAW


# Que es la normalización
# ORDEN CORRECTO PARA DEFINIR UN ATRIBUTO
# CONSULTAS CUENTAME CUANTOS CLIENTES TIENEN UN DNI QUE TERMINA POR LA LETRA "M"
# AGRUPAME LOS NOMBRES QUE SE HAN GASTADO MAS DE X CANTIDAD
# CREAR UNA TABLA CON SUS ATRIBUTOS
# CAMBIAR AL ROL QUE SI TIENE VISIBILIDAD A LA BASE DE DATOS

# Para el examen algo de roles
```sql
SELECT USER AS Rol
FROM mysql.user
WHERE HOST = '%'
    AND NOT LENGTH(authentication_string);
```


# Como registrar un nuevo usuario en MySQL
En este caso solo permitimos la conexión desde localhost,
pero tenemos varias opciones, como por ejemplo '%', que significa
que desde cualquier IP se puede conectar con este usuario y contraseña.

También podemos poner '192.168' y esto quiere decir que solo podrá acceder desde una red privada.
```sql
CREATE USER 'escartii'@'localhost' IDENTIFIED BY 'root';
```

## Modificar usuarios en MySQL

Renombramos el usuario escartii por escartii2
```sql
RENAME USER 'escartii'@'localhost' TO 'escartii2'@'localhost';
```

Cambiar la contraseña de un usuario:
```sql
ALTER USER 'escartii2'@'localhost' IDENTIFIED BY 'root2';
```

Actualizar el host de un usuario y solo podré acceder desde esa IP.
```sql
RENAME USER 'escartii2'@'localhost' TO 'escartii2'@'192.168.5.9';
```

# Eliminar un usuario 
```sql
DROP USER 'escartii'@'localhost';
```


# Como otorgar permisos a un usuario

Ejemplo de uso:
```sql
GRANT [Tipo de permiso] ON [Nombre de base de datos].[Nombre de tabla] TO [Usuario]
```

Vamos a otorgar TODOS los permisos al usuario **escartii** que acabamos de crear.
```sql
GRANT ALL PRIVILEGES ON *.* TO 'escartii'@'localhost';
FLUSH PRIVILEGES;
```

Vamos a otorgar ciertos permisos (Seleccionar, insertar, borrar tablas, actualizar y borrar datos) al usuario sobre una base de datos.
(La base de datos Padel es de mi proyecto Java, si quieres acceso pídemelo.)
(pd: te haría un enlace pero está mi repo privado).
```sql
GRANT SELECT,INSERT,DROP,UPDATE,DELETE ON Padel.* TO 'escartii'@'localhost';
```

# Añadir Rol a un usuario
primero creamos un Rol
```sql
CREATE ROLE 'sololectura'@'localhost';
```
Asignamos el rol al usuario
```sql
GRANT 'sololectura'@'localhost' TO 'escartii'@'localhost'
```
Aplicar el sol
```sql
SET ROLE 'sololectura'@'localhost'
# Como eliminar un usuario
```

# Eliminar un usuario 
```sql
DROP USER 'escartii'@'localhost';
```

Ahora vamos a revocar o quitar permisos a un usuario porque la ha liado parda en producción.

```sql
REVOKE UPDATE, DELETE ON *.* FROM 'escartii'@'localhost'
```

Quitar todos los permisos
```sql
REVOKE ALL PRIVILEGES ON *.*  FROM 'escartii'@'localhost'
```

# Como se autentican los usuarios y qué opciones de autenticación nos permite el SGBD.

por mirar.


# Cómo mostrar los usuarios existentes y sus permisos.

Vamos a listar todos los usuarios que existen y desde qué host se conectan.
```sql
SELECT USER,HOST FROM MYSQL.USER;
```
Por defecto si usamos **SHOW GRANTS;** nos va a mostrar los permisos que tiene el usuario (o rol) que está actualmente conectado, pero si queremos buscar los permisos de otro usuario, lo haremos de esta manera:
```sql
SHOW GRANTS FOR 'escartii2'@'localhost';
```

Cómo saber que usuario está conectado
```sql
SELECT CURRENT_USER();
```


# Qué permisos puede tener un usuario y su nivel de granularidad nos ofrece el SGBD.

PERMITE SEPARAR USUARIOS CON ROLES, define hilar, separar 

```sql
CREATE permite crear nuevas tablas o bases de datos.
DROP permite eliminar tablas o bases de datos.
DELETE permite eliminar registros de tablas.
INSERT permite insertar registros en tablas.
SELECT permite leer registros en las tablas.
UPDATE permite actualizar registros en las tablas.
GRANT OPTION permite remover permisos de usuarios.
SHOW DATABASE permite listar las bases de datos existentes.
ALTER permite al usuario modificar la estructura de la base de datos.
REVOKE permite remover los permisos también.
REFERENCES permite crear claves foráneas.
```

Falta defininir la granularidad.







