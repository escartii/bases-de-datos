SELECT G.Name AS "Género", COUNT(T.TrackId) AS "Número canciones"
FROM Genre G
JOIN Track T
ON G.GenreId = T.GenreId
JOIN InvoiceLine INV
ON T.TrackId = INV.TrackId
GROUP BY G.GenreId;

GRANT ALL PRIVILEGES ON padel.* TO 'root'@'localhost';
SET PASSWORD FOR 'root'@'localhost' = PASSWORD('root');
