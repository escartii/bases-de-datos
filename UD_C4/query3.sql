SELECT 
    FirstName,
    LastName
FROM 
    Employee e
WHERE 
    EXISTS (
        SELECT 1
        FROM 
            Customer c
        WHERE 
            c.SupportRepId = e.EmployeeId
    );
