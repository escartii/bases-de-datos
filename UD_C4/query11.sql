SELECT 
    TrackId,
    Name AS TrackName,
    Milliseconds,
    (SELECT AVG(Milliseconds) FROM Track) AS AverageDuration
FROM 
    Track
WHERE 
    Milliseconds > (SELECT AVG(Milliseconds) FROM Track);

