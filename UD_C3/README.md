# Reto 2: Consultas Empresa y Videoclub

Álvaro Escartí - Estudiante 1º DAW

En este reto trabajamos con las base de datos `Videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/escartii/bases-de-datos/

<div align="center">

## Empresa

</div>

## Query 1

Utilizo JOIN para combinar los datos de la tabla PELICULA con los de la tabla GENERE basándose en la igualdad de los valores de la columna CodiGenere en ambas tablas.
```sql
SELECT Titol, Descripcio
FROM PELICULA
JOIN GENERE ON PELICULA.CodiGenere = GENERE.CodiGenere;
```

## Query 2

En esta query hago lo mismo que arriba, hago un join para combinar los datos de ambas tablas. Esto vincula las facturas con los clientes a través de la clave foránea.
``` sql
SELECT CLIENT.DNI,nom,adreca,telefon,codiFactura,Data,Import
FROM CLIENT
-- cojo el dni porque es el atributo que comparte varias tablas, llamado clave foranea
JOIN FACTURA ON CLIENT.DNI = FACTURA.DNI
WHERE NOM like "Maria%"
```

## Query 3

Hago lo mismo que la anterior query, hago un join para combinar los datos de ambas tablas de CodiActor.
```sql
SELECT *
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;
```

## Query 4

La consulta utiliza la cláusula JOIN para combinar la tabla PELICULA (alias P) con la tabla INTERPRETADA, JOIN para combinar el resultado de la combinación anterior con la tabla ACTOR.
Esta consulta combina datos de tres tablas: PELICULA, INTERPRETADA y ACTOR, utilizando las cláusulas JOIN para relacionarlas mediante las claves primarias y foráneas 
```sql
SELECT *
FROM PELICULA AS P
JOIN INTERPRETADA AS I
JOIN ACTOR AS A
ON P.CodiPeli = I.CodiPeli
AND I.CodiActor = A.CodiActor;
```


## Query 5

La consulta está buscando coincidencias entre el código de una película (CodiPeli) y el código de una segunda parte de esa película (SegonaPart), para luego seleccionar el código y el título de la película original.
```sql
SELECT PELICULA.CodiPeli, PELICULA.Titol
FROM PELICULA
JOIN PELICULA AS P 
ON P.CodiPeli = PELICULA.SegonaPart;

```
