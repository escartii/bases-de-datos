SELECT A.Title, COUNT(INV.TrackId) AS "Numero de Compras"
FROM Track AS T
JOIN Album AS A
ON T.AlbumId = A.AlbumId
JOIN InvoiceLine AS INV
ON T.TrackId = INV.TrackId
GROUP BY A.Title
ORDER BY COUNT(INV.TrackId) DESC
LIMIT 6;