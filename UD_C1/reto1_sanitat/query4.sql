SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO AS "Código Empleado", COGNOM, TORN FROM PLANTILLA
WHERE TORN != "N";

-- También se puede hacer así:

WHERE TORN = "M" OR TORN = "T";

-- otra opcion

WHERE TORN IN ("M" , "T");
