# Unidad C0: Recapitulación

**Álvaro Escartí - 1º DAW**

![Como funcionan las bases de datos](image.png)

Vamos a hablar de conceptos básicos así como también el origen de las bases de datos, su estructura, para que sirven los gestores de base de datos, como funciona la arquitectura cliente-servidor y como tener un gestor de base de datos en remoto.

También hablaremos sobre qué es más conveniente, temas de seguridad. Por otro lado, también veremos que bases de datos son relaciones y cuáles no, así como las que son software libre o no. 

## Concepto y origen de las bases de datos
¿Qué son las bases de datos? ¿Qué problemas tratan de resolver? Definición de base de datos.

Como bien indica en su nombre, **una base de datos almacena datos.** Se utilizan para tratar con datos, añadir, seleccionar, consultar, etc.

Antiguamente, los archivos se guardaban en ficheros de texto y no había manera de poder relacionarlos, por esta razón y otras más se crearon las bases de datos. También para tener más control de los datos que hay almacenados.

## Sistemas de gestión de bases de datos
¿Qué es un sistema de gestión de bases de datos (DBMS)? ¿Qué características de acceso a los datos debería proporcionar? Definición de DBMS.

Partimos de un gestor de base de datos le conocemos como un **Software.** El gestor de base de datos se utiliza para almacenar los datos en una estrcutura definida por el usuario. También nos permite modificar o tratar los datos.
Hay dos tipos de gestores de bases de datos:

* Modelo Relacional
* Modelo no Relacional

El gestor de base de datos debería permitirte acceder a los datos a través de interfaz gráfica o bien con un lenguaje de consultas. También debería estar disponible siempre para los usuarios.


### Ejemplos de sistemas de gestión de bases de datos
¿Qué DBMS se usan a día de hoy? ¿Cuáles de ellos son software libre? ¿Cuáles de ellos siguen el modelo cliente-servidor?

* Oracle DB - No es software libre y sigue el modelo cliente-servidor.
* IMB Db2 - No es software libre y sigue el modelo cliente-servidor.
* SQLite - Si es software libre y no sigue el modelo cliente-servidor, ya que es una bbdd sin servidor.
* MariaDB - Si es software libre y sigue el modelo cliente-servidor.
* SQL Server - No es software libre y sigue el modelo cliente-servidor.
* PostgreSQL - Si es software libre y sigue el modelo cliente-servidor.
* mySQL - Es software libre pero Oracle tiene una licencia, sigue el modelo cliente-servidor.

## Modelo cliente-servidor

¿Por qué es interesante que el DBMS se encuentre en un servidor? ¿Qué ventajas tiene desacoplar al DBMS del cliente? ¿En qué se basa el modelo cliente-servidor?

1. Es interesante tener sistema de gestión de bases de datos en el servidor para tener acceso remoto desde cualquier sitio, solo necesitaremos una conexión a Internet y también por medidas de seguridad.


2. DBMS del cliente proporciona ventajas significativas en térmidos de rendimiento, centralización de datos, acceso remoto, seguridad y escalabilidad.

3. El modelo cliente-servidor es un concepto en el desarrollo de software que describe una arquitectura en la que hay un cliente que solicita servicios a un servidor. El cliente se comunica al servidor a través de una red, donde el cliente envía solicitudes al servidor y el servidor responde a esas solicitudes. El servidor siempre escucha por un puerto para servir un recurso.

* __Cliente__: Es la máquina/software que pide un recurso a través de una red.
* __Servidor__: Es la máquina/software que ofrece un servicio a través de la red.
* __Red__: Sitio mediante el que se conectan el cliente y el  servidor 
* __Puerto de escucha__: Por el cual el cliente accede al recurso que ofrece el servidor.
* __Petición__: Es cuando el cliente demanda algo al servidor(Petición).
* __Respuesta__: Es el mensaje que devuelve el servidor cuando recibe la peticion.


## SQL
¿Qué es SQL? ¿Qué tipo de lenguaje es?

SQL, es un lenguaje de consultas. Con él podremos interactuar con la base de datos **relacionales**.Interactuamos con la base de datos con algunos comandos, por ejemplo:

* SELECT: Se utiliza para recuperar datos de la base de datos.
* INSERT: Se utiliza para insertar nuevos datos en la base de datos.
* UPDATE: Se utiliza para modificar datos existentes en la base de datos.
* DELETE: Se utiliza para eliminar datos de la base de datos.
* CREATE TABLE: Se utiliza para crear nuevas tablas en la base de datos.
* ALTER TABLE: Se utiliza para modificar la estructura de una tabla existente.
### Instrucciones de SQL

#### DDL
LA DDL es para establecer la estructura de la base de datos (DATA DEFINITION LANGUAGE). Crear tablas, alter table, etc.
#### DML
DML significa DATA MANIPULATION LANGUAGE y como indica su nombre, podemos insertar datos, actualizar los datos, borrar datos, etc.
#### DCL
DCL significa (Data Control Language). Se utiliza para controlar el acceso a los datos y los objetos de la base de datos, otorgando y revocando permisos a los usuarios.
#### TCL
Hacen referencia a las "Transaction Control Language" (Lenguaje de Control de Transacciones).
    


## Bases de datos relacionales
¿Qué es una base de datos relacional? ¿Qué ventajas tiene? ¿Qué elementos la conforman?

Una base de datos relacional es un tipo de base de datos que organiza los datos en forma de tablas o relaciones. Cada tabla consta de filas y columnas, donde cada fila representa una instancia individual de los datos y cada columna representa un atributo específico de esos datos.

* __Relación (tabla)__: Se representa como una tabla que contiene datos relacionados entre sí. Cada tabla tiene un nombre único y está compuesta por filas y columnas.
* __Atributo/Campo (columna)__: Son las categorías de información que describen los datos en una tabla. 
* __Registro/Tupla (fila)__:Son las instancias individuales de datos almacenadas en una tabla. Cada fila de una tabla representa una entidad única y contiene valores para cada atributo definido en esa tabla.

Ventajas:

* Estructura organizada
* Integridad de los datos
* Flexibilidad 
* Escalabilidad
* Consistencia


