SELECT 
    CustomerId,
    FirstName,
    LastName,
    SUM(Total) AS TotalSpent,
    CASE 
        WHEN SUM(Total) > 500 THEN 'VIP'
        ELSE 'Regular'
    END AS CustomerType
FROM 
    Customer c
JOIN 
    Invoice USING (CustomerId)
GROUP BY 
    CustomerId, FirstName, LastName;
