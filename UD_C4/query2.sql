SELECT DISTINCT
    ar.ArtistId,
    ar.Name AS ArtistName
FROM 
    Artist ar
WHERE 
    ar.ArtistId IN (
        SELECT 
            al.ArtistId
        FROM 
            Album al
        JOIN 
            Track t ON al.AlbumId = t.AlbumId
        WHERE 
            t.Milliseconds > 300000 -- 300000 milisegundos = 5 minutos
    );
