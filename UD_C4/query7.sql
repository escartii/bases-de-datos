SELECT 
    TrackId,
    Name AS TrackName,
    UnitPrice
FROM 
    Track t
WHERE 
    UnitPrice > ALL (
        SELECT 
            t2.UnitPrice
        FROM 
            Track t2
        WHERE 
            t2.AlbumId = t.AlbumId
    );
