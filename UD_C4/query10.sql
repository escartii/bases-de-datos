SELECT 
    t.TrackId,
    t.Name AS TrackName,
    (SELECT COUNT(*)
     FROM InvoiceLine il
     WHERE il.TrackId = t.TrackId) AS TotalPurchases
FROM 
    Track t
ORDER BY 
    TotalPurchases DESC
LIMIT 10;

