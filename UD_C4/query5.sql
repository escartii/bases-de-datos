SELECT 
    e.EmployeeId AS EmployeeID,
    e.FirstName AS EmployeeFirstName,
    e.LastName AS EmployeeLastName,
    e.ReportsTo AS SupervisorID,
    (SELECT CONCAT(FirstName, ' ', LastName) FROM Employee WHERE EmployeeId = e.ReportsTo) AS SupervisorName
FROM 
    Employee e
ORDER BY 
    e.ReportsTo, e.EmployeeId;
