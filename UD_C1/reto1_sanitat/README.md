# Reto 1: Consultas básicas

Álvaro Escartí - Estudiante 1º DAW

En este reto trabajamos con la base de datos `sanitat`, que nos viene dada en el fichero `sanitat.sql`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/la-senia-db-2024/db/

## Query 1
Para seleccionar el número, nombre y teléfono de todos los hospitales existentes, seleccionaremos estos tres atributos, que se corresponden con las columnas `HOSPITAL_COD`, `NOM`, y `TELEFON`, respectivamente, de la tabla `HOSPITAL`. Lo llevaremos a cabo con la siguiente sentencia SQL:

```sql
SELECT HOSPITAL_COD AS "Código Hospital", NOM AS "Nombre Hospital" , TELEFON 
FROM HOSPITAL;
```

## Query 2

Usamos select para seleccionar el `código de hospital`, `nombre (del hopistal)`, y `telefono de la tabla`  `HOSPITAL.`
Utilizo la función substring y le paso como parámetro la columna que quiero seleccionar en este caso "NOM"
el segundo parámetro es la posición a partir que quiero contar y el último parámetro a partir del segundo parámetro cuántos carácteres cogemos.

``` sql
SELECT HOSPITAL_COD, NOM, TELEFON FROM HOSPITAL
-- Busco de la cadena cortada, y me quedo con el resultado
WHERE SUBSTRING(NOM,2,1) = "a";

```

## Query 3
```sql
SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO AS "Código Empleado", COGNOM FROM PLANTILLA;
```

## Query 4
```sql
SELECT HOSPITAL_COD,SALA_COD,EMPLEAT_NO AS "Código Empleado", COGNOM, TORN FROM PLANTILLA
WHERE TORN != "N";
```
Otra opción de hacerlo:

```sql
WHERE TORN = "M" OR TORN = "T";
```

Otra opción más:
```sql
WHERE TORN IN ("M" , "T");
```

## Query 5
```sql
SELECT COGNOM, ADRECA, DATA_NAIX,SEXE FROM MALALT
WHERE YEAR (DATA_NAIX) = (1960)
```

## Query 6
```sql
SELECT COGNOM, ADRECA, DATA_NAIX,SEXE FROM MALALT
WHERE YEAR (DATA_NAIX) >= (1960)
```

otra opción de hacerlo:
```sql
WHERE DATA_NAIX > DATE("1960-01-01");
```
