SELECT TrackId, Name
FROM Track
WHERE TrackId NOT IN (
    SELECT TrackId
    FROM InvoiceLine
);

