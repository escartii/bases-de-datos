#!/bin/bash

# Solicitar al usuario el número de archivos a crear
read -p "Ingrese el número de archivos que desea crear: " num_archivos

# Loop para crear los archivos
for ((i = 1; i <= num_archivos; i++)); do
    filename="query${i}.sql"
    echo "Creando archivo ${filename}..."
    echo "query${i}" > "${filename}"
done

echo "Archivos creados correctamente."

