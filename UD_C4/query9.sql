SELECT 
    c.CustomerId,
    c.FirstName,
    c.LastName
FROM 
    Customer c
WHERE 
    c.CustomerId IN (
        SELECT 
            i.CustomerId
        FROM 
            Invoice i
        JOIN 
            InvoiceLine il ON i.InvoiceId = il.InvoiceId
        GROUP BY 
            i.CustomerId, i.InvoiceId
        HAVING 
            COUNT(il.TrackId) > 20
    );

