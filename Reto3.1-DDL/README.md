## Apuntes Álvaro Escartí - 1º DAW

## Como conectarse a una base de datos desde (CLI) la linea de comandos.
```bash
mysql -u root -p -h 127.0.0.1 -P 33006
```

## Mostrar todas las tablas que contiene una base de datos
```sql
SHOW TABLES FROM Chinook;
```
O bien de esta manera también podríamos hacerlo
```sql
USE Chinook;
SHOW TABLES;
```

## Mostrar las columnas de una tabla
```sql
SHOW COLUMNS FROM Album;
```
De una manera más corta, pero hace lo mismo:
```sql
DESCRIBE Album;
```
## Cómo saber que base de datos hemos seleccionado.
```sql
SELECT DATABASE();
```
## Qué es un commit en MySQL?
Básicamente es parecido a git en este sentido, cuando tenemos autocommit activado, es decir en 0, todos los cambios que hagamos en las tablas se harán automáticamente.
Sin embargo, si el autocommit está en 1 hasta que no ejecutemos la instrucción **COMMIT** no habrá guardado nada desde el último commit.

## Comprobar si tenemos activado el autocommit.
```sql
SELECT @@autocommit;
```
Si el autocommit está en 1 significa que esta desactivado, si queremos activarlo, debemos realizar esto: 
```sql
SET autocommit=0;
```

## Guardar los cambios con un commit
```sql
COMMIT;
```

## Volver al último commit.
```sql
ROLLBACK;
```

## Crear un esquema
```sql
CREATE SCHEMA Pruebas;
```
## Crear base de datos
```sql
CREATE DATABASE IF NOT EXISTS vuelos;
```

## Crear tablas pasajeros

Explicación de cada campo:

**id_pasajero** : Considero que debe ser un número entero y no puede ser nulo. Es obligatorio tenerlo. 

**numero_pasaporte** : Considero que con 20 carácteres debería ser suficiente para un pasaporte y tampoco puede ser nulo.

**nombre_pasajero** : Considero que con 20 carácteres es suficiente para un nombre y tampoco puede ser nulo.

**Para este ejemplo solo he añadido estas tres columnas, el pasajero que será autroincremental, un número de pasaporte y el nombre del pasajero.**


```sql
CREATE TABLE `pasajeros` (
  `id_pasajero` int NOT NULL AUTO_INCREMENT,
  `numero_pasaporte` varchar(20) NOT NULL,
  `nombre_pasajero` varchar(20) NOT NULL,
  PRIMARY KEY (`id_pasajero`)
)
```


## Creación de la tabla Vuelos

Explicación de cada campo:

**id_vuelo** : es un número entero, que no puede ser nulo y será autoincremental.

**numero_vuelo** : es una cadena de carácteres (15) y no podrá ser nulo, por ejemplo: vuelo KLZ937

**origen** : No puede ser nulo para saber a qué dirección va ese avión.

**destino** : lo mismo que para el origen.

**fecha** : el campo será DATE que será una fecha y no puede ser nula, para saber que cuando vuela el avión.

**capacidad** : no puede ser nula, debemos saber la capacidad del avión para poder comprobar si quedan billetes disponibles.

```sql
CREATE TABLE `vuelos` (
  `id_vuelo` int NOT NULL AUTO_INCREMENT,
  `numero_vuelo` varchar(15) NOT NULL,
  `origen` varchar(50) NOT NULL,
  `destino` varchar(50) NOT NULL,
  `fecha` date NOT NULL,
  `capacidad` int NOT NULL,
  PRIMARY KEY (`id_vuelo`)
)
```

## Creación de la tabla vuelos_pasajeros


Al ser una relación de muchos a muchos es decir, un vuelo puede ser reservado por muchos pasajeros y muchos pasajeros pueden reservar un avión. Para ello creamos una tabla intermedia con dos claves foráneas para relacionar las tablas.

**id_Vuelos_Pasajeros** : Este campo es un número entero para cada registro que inserte en la base de datos. Tampoco puede ser nulo.

**id_vuelo** : Este campo es una clave foránea con la tabla **VUELOS** 

**id_pasajero** : Este campo es una clave foránea con la tabla **PASAJEROS** 

**n_asiento** : Este campo tiene un varchar de 3 carácteres para añadir el asiento, **por ejemplo 21A**

**Restricción CONSTRAINT id_vuelo** : Con esto garantizamos la **INTEGRIDAD DE LOS DATOS** esto quiere decir que debe existir un valor en la tabla id_vuelo, por ejemplo: quiero reservar el id_vuelo 37 pero no existe, entonces no podremos crear una reserva a un vuelo que no existe.

**Restricción CONSTRAINT id_pasajero** : Lo mismo que la anterior, no podemos reservar un vuelo para un pasajero si no existe el pasajero, por ejemplo; quiero reservar un vuelo para el id_pasajero 99, pero este no existe, entonces no podemos crear la reserva.
```sql
CREATE TABLE `vuelos_pasajeros` (
  `id_Vuelos_Pasajeros` int NOT NULL AUTO_INCREMENT,
  `id_vuelo` int DEFAULT NULL,
  `id_pasajero` int DEFAULT NULL,
  `n_asiento` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_Vuelos_Pasajeros`),
  KEY `id_vuelo` (`id_vuelo`),
  KEY `id_pasajero` (`id_pasajero`),
  CONSTRAINT `vuelos_pasajeros_ibfk_1` FOREIGN KEY (`id_vuelo`) REFERENCES `vuelos` (`id_vuelo`),
  CONSTRAINT `vuelos_pasajeros_ibfk_2` FOREIGN KEY (`id_pasajero`) REFERENCES `pasajeros` (`id_pasajero`)
)
