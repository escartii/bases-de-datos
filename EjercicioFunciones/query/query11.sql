SELECT FirstName AS "Nombre", LastName AS "Apellido"
FROM Customer C
JOIN Invoice I
ON C.CustomerId = I.CustomerId
WHERE I.Total > '10'
ORDER BY C.LastName DESC;