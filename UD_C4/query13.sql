SELECT 
    t.TrackId,
    t.Name AS TrackName,
    COUNT(il.InvoiceLineId) AS SalesCount,
    (SELECT AVG(SalesCount) FROM (SELECT COUNT(il2.InvoiceLineId) AS SalesCount FROM InvoiceLine il2 GROUP BY il2.TrackId) AS AvgSalesPerTrack) AS AvgSalesPerTrack
FROM 
    Track t
JOIN 
    InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY 
    t.TrackId, t.Name
HAVING 
    COUNT(il.InvoiceLineId) > (SELECT AVG(SalesCount) FROM (SELECT COUNT(il2.InvoiceLineId) AS SalesCount FROM InvoiceLine il2 GROUP BY il2.TrackId) AS AvgSalesPerTrack);

