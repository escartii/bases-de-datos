# Reto 4: Consultas Chinook

Álvaro Escartí - Estudiante 1º DAW

En este reto trabajamos con las base de datos `Chinook`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/escartii/bases-de-datos/

<div align="center"> </div>

## Empresa

## Query 1

Primero, selecciono ciertos campos de estas tablas. Luego, une estas tablas utilizando la cláusula JOIN.

La cláusula WHERE filtra los resultados para incluir solo las playlists cuyo nombre comienza con 'M' y las pistas que están entre las tres más baratas de su álbum.

```sql
SELECT 
    pl.PlaylistId,
    pl.Name AS PlaylistName,
    t.TrackId,
    t.Name AS TrackName,
    t.AlbumId,
    a.Title AS AlbumTitle,
    t.UnitPrice
FROM 
    Playlist pl
JOIN 
    PlaylistTrack plt ON pl.PlaylistId = plt.PlaylistId
JOIN 
    Track t ON plt.TrackId = t.TrackId
JOIN 
    Album a ON t.AlbumId = a.AlbumId
WHERE 
    pl.Name LIKE 'M%'
    AND (
        SELECT 
            COUNT(*)
        FROM 
            Track t2
        WHERE 
            t2.AlbumId = t.AlbumId
            AND t2.UnitPrice <= t.UnitPrice
    ) <= 3
ORDER BY 
    a.Title, t.UnitPrice;
```

## Query 2

selecciono los artistas que tienen álbumes con al menos una canción que dura más de 5 minutos.
``` sql
SELECT DISTINCT
    ar.ArtistId,
    ar.Name AS ArtistName
FROM 
    Artist ar
WHERE 
    ar.ArtistId IN (
        SELECT 
            al.ArtistId
        FROM 
            Album al
        JOIN 
            Track t ON al.AlbumId = t.AlbumId
        WHERE 
            t.Milliseconds > 300000 -- 300000 milisegundos = 5 minutos
    );

```

## Query 3

Selecciono los nombres de los empleados que están asignados como representantes de soporte para al menos un cliente.
```sql
SELECT 
    FirstName,
    LastName
FROM 
    Employee e
WHERE 
    EXISTS (
        SELECT 1
        FROM 
            Customer c
        WHERE 
            c.SupportRepId = e.EmployeeId
    );

```

## Query 4

Selecciono el ID y el nombre de las pistas que no han sido compradas en ninguna transacción.
```sql
SELECT TrackId, Name
FROM Track
WHERE TrackId NOT IN (
    SELECT TrackId
    FROM InvoiceLine
);


```
## Query 5

Selecciono el ID, el nombre y el apellido de cada empleado, junto con el ID de su supervisor y el nombre completo del supervisor. Los resultados están ordenados por el ID del supervisor y el ID del empleado.
```sql
SELECT 
    e.EmployeeId AS EmployeeID,
    e.FirstName AS EmployeeFirstName,
    e.LastName AS EmployeeLastName,
    e.ReportsTo AS SupervisorID,
    (SELECT CONCAT(FirstName, ' ', LastName) FROM Employee WHERE EmployeeId = e.ReportsTo) AS SupervisorName
FROM 
    Employee e
ORDER BY 
    e.ReportsTo, e.EmployeeId;


```

## Query 6

Selecciono el ID y el nombre de las canciones que han sido compradas por el cliente llamado Luis Rojas.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName
FROM 
    Track t
WHERE 
    t.TrackId IN (
        SELECT 
            il.TrackId
        FROM 
            InvoiceLine il
        JOIN 
            Invoice i ON il.InvoiceId = i.InvoiceId
        JOIN 
            Customer c ON i.CustomerId = c.CustomerId
        WHERE 
            c.FirstName = 'Luis' AND c.LastName = 'Rojas'
    );


```
## Query 7

selecciono el ID, nombre y precio de las canciones que tienen un precio mayor que el precio de cualquier otra canción en el mismo álbum.
```sql
SELECT 
    TrackId,
    Name AS TrackName,
    UnitPrice
FROM 
    Track t
WHERE 
    UnitPrice > ALL (
        SELECT 
            t2.UnitPrice
        FROM 
            Track t2
        WHERE 
            t2.AlbumId = t.AlbumId
    );


```
## Query 9

Selecciono el ID, nombre y apellido de los clientes que han comprado más de 20 canciones en una sola transacción.
```sql
SELECT 
    c.CustomerId,
    c.FirstName,
    c.LastName
FROM 
    Customer c
WHERE 
    c.CustomerId IN (
        SELECT 
            i.CustomerId
        FROM 
            Invoice i
        JOIN 
            InvoiceLine il ON i.InvoiceId = il.InvoiceId
        GROUP BY 
            i.CustomerId, i.InvoiceId
        HAVING 
            COUNT(il.TrackId) > 20
    );



```
## Query 10

Selecciono el ID y el nombre de las canciones, junto con el recuento total de compras de cada canción. Luego, ordena los resultados en orden descendente por el total de compras y limita los resultados a las 10 canciones más compradas.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName,
    (SELECT COUNT(*)
     FROM InvoiceLine il
     WHERE il.TrackId = t.TrackId) AS TotalPurchases
FROM 
    Track t
ORDER BY 
    TotalPurchases DESC
LIMIT 10;



```
## Query 11

Selecciono el ID y el nombre de las canciones, junto con su duración en milisegundos. También calcula la duración promedio de todas las canciones y luego selecciona solo aquellas canciones cuya duración sea mayor que la duración promedio calculada.
```sql
SELECT 
    TrackId,
    Name AS TrackName,
    Milliseconds,
    (SELECT AVG(Milliseconds) FROM Track) AS AverageDuration
FROM 
    Track
WHERE 
    Milliseconds > (SELECT AVG(Milliseconds) FROM Track);

```
## Query 13


Selecciono el ID y el nombre de las canciones, así como el número total de ventas de cada canción. Luego, compara este número con el promedio de ventas por canción. Si el número de ventas de una canción es mayor que el promedio de ventas por canción, esa canción se incluye en los resultados.
```sql
SELECT 
    t.TrackId,
    t.Name AS TrackName,
    COUNT(il.InvoiceLineId) AS SalesCount,
    (SELECT AVG(SalesCount) FROM (SELECT COUNT(il2.InvoiceLineId) AS SalesCount FROM InvoiceLine il2 GROUP BY il2.TrackId) AS AvgSalesPerTrack) AS AvgSalesPerTrack
FROM 
    Track t
JOIN 
    InvoiceLine il ON t.TrackId = il.TrackId
GROUP BY 
    t.TrackId, t.Name
HAVING 
    COUNT(il.InvoiceLineId) > (SELECT AVG(SalesCount) FROM (SELECT COUNT(il2.InvoiceLineId) AS SalesCount FROM InvoiceLine il2 GROUP BY il2.TrackId) AS AvgSalesPerTrack);


```
## Query 15

Selecciono el ID de la factura, el total de la factura y calcula el descuento aplicado sobre cada factura. Si el total de la factura es mayor que $100, se aplica un descuento del 10%; de lo contrario, no se aplica ningún descuento.
```sql
SELECT 
    InvoiceId,
    Total,
    CASE 
        WHEN Total > 100 THEN Total * 0.1
        ELSE 0
    END AS Discount
FROM 
    Invoice;


```
## Query 16

Selecciono el ID, nombre, apellido y título de cada empleado, además de asignar un nivel (Manager, Assistant o Empleado) basado en el título del empleado.
```sql
SELECT 
    EmployeeId,
    FirstName,
    LastName,
    Title,
    CASE 
        WHEN Title LIKE '%Manager%' THEN 'Manager'
        WHEN Title LIKE '%Assistant%' THEN 'Assistant'
        ELSE 'Empleado'
    END AS Level
FROM 
    Employee;

```
## Query 17

Selecciono el ID, nombre y apellido de cada cliente, junto con el total gastado por el cliente. Además, asigna una etiqueta de "VIP" o "Regular" al cliente según el total gastado.
```sql
SELECT 
    CustomerId,
    FirstName,
    LastName,
    SUM(Total) AS TotalSpent,
    CASE 
        WHEN SUM(Total) > 500 THEN 'VIP'
        ELSE 'Regular'
    END AS CustomerType
FROM 
    Customer c
JOIN 
    Invoice USING (CustomerId)
GROUP BY 
    CustomerId, FirstName, LastName;

```
