# Reto 2: Consultas Empresa y Videoclub

Álvaro Escartí - Estudiante 1º DAW

En este reto trabajamos con las base de datos `Empresa` y `Videoclub`. A continuación realizamos una serie de consultas para extraer la información pedida en cada uno de los enunciados.

El código fuente correspondiente a este reto puede consultarse en: https://gitlab.com/escartii/bases-de-datos/

<div align="center">

## Empresa

</div>

## Query 1

La Query 1 es una consulta SQL que selecciona todos los registros de la tabla "producte"
```sql
SELECT * 
FROM producte;
```

## Query 2

 Esta consulta SQL selecciona los campos "PROD_NUM" y "DESCRIPCIO" de la tabla "producte" donde el valor de "DESCRIPCIO" contiene la palabra "tennis

``` sql
SELECT PROD_NUM, DESCRIPCIO 
FROM producte
WHERE DESCRIPCIO LIKE '%tennis%';
```

## Query 3

La Query 3 es una consulta SQL que selecciona tres campos de la tabla "client": "NOM", "AREA" y "TELEFON".

```sql
SELECT NOM,AREA,TELEFON 
FROM client;
```

## Query 4

La Query 4 es una consulta SQL que selecciona tres campos de la tabla "client": "CLIENT_COD", "NOM" y "CIUTAT". Uso WHERE para filtrar los resultados y mostrar solo aquellos registros donde el valor de "area" sea distinto a 636.

```sql
SELECT CLIENT_COD,NOM,CIUTAT 
FROM client
WHERE area != 636;
```


## Query 5

selecciono tres campos de la tabla "comanda": "CLIENT_COD", "COM_DATA" y "DATA_TRAMESA para ver las compras realizadas.

```sql
SELECT CLIENT_COD, COM_DATA, DATA_TRAMESA 
FROM comanda;
```


<div align="center">

## VideoClub

</div>

## Query 6

Hago una consulta a los campos Nombre y telefono de la tabla clientes. Para obtener información de los clientes.

```sql
SELECT NOM, TELEFON 
FROM client;
```

## Query 7

Esta Query selecciono los campos Data (Fecha) e Import (Importe) de la tabla factura, para ver en que fecha se ha realizado la factura y su importe.

```sql
SELECT Data, Import 
FROM factura;
```

## Query 8

Selecciono el campo "Descripcio" de la tabla "detallfactura" donde el valor del campo "CodiFactura" es igual a 3.

```sql
SELECT Descripcio 
FROM detallfactura
WHERE CodiFactura = 3;
```

## Query 9

Selecciono todos los campos de la tabla "factura" y los ordena en orden descendente según el valor del campo "import".

```sql
SELECT * 
FROM factura
ORDER BY `import` DESC;
```


## Query 10

Selecciono el campo "Nom" de la tabla "actor" donde el primer carácter del campo "Nom" es igual a "X".

```sql
SELECT Nom 
FROM actor
WHERE SUBSTRING(NOM,1,1)  = "X";
```
