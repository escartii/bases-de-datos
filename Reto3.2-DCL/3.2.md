# README: Control de Acceso en MySQL

## Introducción
Este documento proporciona una guía sobre las opciones de control de acceso en MySQL. Se detallan los procesos para registrar, modificar y eliminar usuarios, las opciones de autenticación, cómo visualizar los usuarios y sus permisos, los niveles de granularidad de los permisos, los requisitos para gestionar usuarios y permisos, y la posibilidad de agrupar usuarios mediante roles.

## Registro, Modificación y Eliminación de Usuarios

### Registro de Nuevos Usuarios
Para crear un nuevo usuario en MySQL:
```sql
CREATE USER 'nuevo_usuario'@'localhost' IDENTIFIED BY 'contraseña';
```

### Modificación de Usuarios
Para modificar un usuario existente:
- **Cambiar contraseña**:
  ```sql
  ALTER USER 'usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña';
  ```
- **Renombrar usuario**:
  ```sql
  RENAME USER 'usuario'@'localhost' TO 'nuevo_usuario'@'localhost';
  ```

### Eliminación de Usuarios
Para eliminar un usuario:
```sql
DROP USER 'usuario'@'localhost';
```

## Autenticación de Usuarios

### Autenticación de Usuarios
MySQL permite varias opciones de autenticación:
- **Autenticación basada en contraseña**:
  ```sql
  CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'contraseña';
  ```
- **Autenticación de plugins**: MySQL soporta plugins de autenticación como `mysql_native_password`, `caching_sha2_password`, entre otros. Ejemplo:
  ```sql
  CREATE USER 'usuario'@'localhost' IDENTIFIED WITH 'caching_sha2_password' BY 'contraseña';
  ```

### Opciones de Autenticación
- **mysql_native_password**: Método de autenticación antiguo, pero aún soportado.
- **caching_sha2_password**: Método más seguro y recomendado en versiones recientes de MySQL.
- **auth_socket**: Permite a los usuarios autenticarse mediante el socket del sistema operativo, sin necesidad de contraseña.

## Visualización de Usuarios y Permisos

### Mostrar Usuarios Existentes
Para mostrar todos los usuarios:
```sql
SELECT user, host FROM mysql.user;
```

### Mostrar Permisos de un Usuario
Para mostrar los permisos de un usuario específico:
```sql
SHOW GRANTS FOR 'usuario'@'localhost';
```

## Permisos de Usuario y Granularidad

### Tipos de Permisos
- **Permisos globales**: Aplicables a todas las bases de datos del servidor.
- **Permisos de base de datos**: Aplicables a todas las tablas de una base de datos específica.
- **Permisos de tabla**: Aplicables a tablas específicas.
- **Permisos de columna**: Aplicables a columnas específicas dentro de una tabla.
- **Permisos de procedimiento**: Aplicables a procedimientos almacenados y funciones.

### Ejemplos de Permisos
- **Global**:
  ```sql
  GRANT ALL PRIVILEGES ON *.* TO 'usuario'@'localhost';
  ```
- **Base de datos específica**:
  ```sql
  GRANT SELECT, INSERT ON nombre_base_datos.* TO 'usuario'@'localhost';
  ```
- **Tabla específica**:
  ```sql
  GRANT SELECT, INSERT ON nombre_base_datos.nombre_tabla TO 'usuario'@'localhost';
  ```
- **Columna específica**:
  ```sql
  GRANT SELECT (columna1, columna2) ON nombre_base_datos.nombre_tabla TO 'usuario'@'localhost';
  ```

## Gestión de Permisos de Usuarios

### Permisos Necesarios para Gestionar Usuarios
Para gestionar usuarios y sus permisos, se necesitan permisos administrativos como:
- **CREATE USER**
- **DROP USER**
- **GRANT OPTION**
- **REVOKE**
- **ALTER USER**

Ejemplo para otorgar permisos administrativos:
```sql
GRANT CREATE USER, DROP USER, GRANT OPTION, REVOKE, ALTER ON *.* TO 'admin_usuario'@'localhost';
```

## Agrupación de Usuarios (Roles)

### Uso de Roles en MySQL
MySQL permite la creación de roles para agrupar permisos y asignarlos a usuarios.
- **Crear un rol**:
  ```sql
  CREATE ROLE 'nombre_rol';
  ```
- **Asignar permisos a un rol**:
  ```sql
  GRANT SELECT, INSERT ON nombre_base_datos.* TO 'nombre_rol';
  ```
- **Asignar un rol a un usuario**:
  ```sql
  GRANT 'nombre_rol' TO 'usuario'@'localhost';
  ```
- **Activar un rol para un usuario**:
  ```sql
  SET DEFAULT ROLE 'nombre_rol' TO 'usuario'@'localhost';
  ```

## Comandos para Gestionar Usuarios y Permisos

### Comandos Clave
- **Crear usuario**:
  ```sql
  CREATE USER 'usuario'@'localhost' IDENTIFIED BY 'contraseña';
  ```
- **Modificar usuario**:
  ```sql
  ALTER USER 'usuario'@'localhost' IDENTIFIED BY 'nueva_contraseña';
  ```
- **Eliminar usuario**:
  ```sql
  DROP USER 'usuario'@'localhost';
  ```
- **Mostrar usuarios**:
  ```sql
  SELECT user, host FROM mysql.user;
  ```
- **Mostrar permisos de usuario**:
  ```sql
  SHOW GRANTS FOR 'usuario'@'localhost';
  ```
- **Asignar permisos**:
  ```sql
  GRANT SELECT, INSERT ON nombre_base_datos.* TO 'usuario'@'localhost';
  ```
- **Revocar permisos**:
  ```sql
  REVOKE SELECT, INSERT ON nombre_base_datos.* FROM 'usuario'@'localhost';
  ```
- **Crear rol**:
  ```sql
  CREATE ROLE 'nombre_rol';
  ```
- **Asignar permisos a un rol**:
  ```sql
  GRANT SELECT, INSERT ON nombre_base_datos.* TO 'nombre_rol';
  ```
- **Asignar rol a un usuario**:
  ```sql
  GRANT 'nombre_rol' TO 'usuario'@'localhost';
  ```

## Conclusión
Esta guía proporciona una visión general sobre el control de acceso en MySQL, abarcando desde la gestión básica de usuarios hasta el uso de roles para agrupar permisos. Implementar estas prácticas ayuda a mantener la seguridad y la integridad de los datos en el sistema de gestión de bases de datos.