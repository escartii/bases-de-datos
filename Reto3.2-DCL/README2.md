# Reto 3.2 DATA CONTROL LANGUAGE (DCL) - Álvaro Escartí 1º DAW 👨🏽‍💻

[Mi repositorio de GitLab](https://gitlab.com/escartii/bases-de-datos)

## Índice
1. [Registro de nuevos usuarios](#registro-de-nuevos-usuarios)
2. [Modificación y eliminación de usuarios](#modificación-y-eliminación-de-usuarios)
3. [Autenticación de usuarios](#autenticación-de-usuarios)
4. [Mostrar usuarios existentes y sus permisos](#mostrar-usuarios-existentes-y-sus-permisos)
5. [Permisos de usuarios](#permisos-de-usuarios)
6. [Gestión de permisos y usuarios](#gestión-de-permisos-y-usuarios)
7. [Agrupación de usuarios](#agrupación-de-usuarios)
8. [Comandos para gestionar usuarios y permisos](#comandos-para-gestionar-usuarios-y-permisos)

## Registro de nuevos usuarios

Para registrar nuevos usuarios en MySQL, se utiliza el comando **CREATE USER**.

### Ejemplo:
Opción 1: Creamos un usuario que solo puede acceder desde localhost.
Opción 2: Creamos un usuario que puede acceder desde cualquier IP.
Opción 3: Creamos un usuario que puede acceder solamente desde la subred 5.

```sql
CREATE USER 'escartii'@'localhost' IDENTIFIED BY 'root';
CREATE USER 'escartii'@'%' IDENTIFIED BY 'root';
CREATE USER 'escartii'@'192.168.5' IDENTIFIED BY 'root'
```

## Modificación y eliminación de usuarios

Para modificar las propiedades de un usuario, como su contraseña o permisos, se utilizan los comandos **ALTER USER** o **GRANT**.

#### Ejemplo:
```sql
ALTER USER 'escartii'@'localhost' IDENTIFIED BY 'root1234';
GRANT SELECT, INSERT ON base_de_datos.* TO 'usuario_existente'@'localhost';
```

### Eliminación de usuarios

Para eliminar un usuario, se utiliza el comando **DROP USER**.

#### Ejemplo:
```sql
DROP USER 'escartii'@'localhost';
```

## Autenticación de usuarios

La autenticación de usuarios en MySQL como generalmente usamos es por contraseñas y también es posible requerir conexiones seguras con un certificado SSL.

### Opciones de autenticación:
- **Autenticación mediante contraseña**: Es la que usamos generalmente y requiere insertar la contraseña.
- **Autenticación SSL**: Utiliza certificados SSL/TLS para autenticar usuarios.

### Para iniciar sesión (luego nos pedirá la contraseña)
mysql -u escartii -p 
#### Ejemplo:
```sql
CREATE USER 'usuario_ssl'@'localhost' REQUIRE SSL;
```

## Mostrar usuarios existentes (también puede ser un ROL) y sus permisos

```sql
SELECT user, host FROM mysql.user;
SHOW GRANTS FOR 'usuario_existente'@'localhost';
```

## Permisos de usuarios

### Tipos de permisos:
- **Permisos de acceso**: SELECT, INSERT, UPDATE, DELETE.
- **Permisos administrativos**: CREATE, DROP, GRANT OPTION.
- **Permisos específicos sobre una tabla**: ALL PRIVILEGES ON base_de_datos.tabla.

### Los más útiles para el examen:

```sql
CREATE permite crear nuevas tablas o bases de datos.
DROP permite eliminar tablas o bases de datos.
DELETE permite eliminar registros de tablas.
INSERT permite insertar registros en tablas.
SELECT permite leer registros en las tablas.
UPDATE permite actualizar registros en las tablas.
GRANT OPTION permite remover permisos de usuarios.
SHOW DATABASE permite listar las bases de datos existentes.
ALTER permite al usuario modificar la estructura de la base de datos.
REVOKE permite remover los permisos también.
REFERENCES permite crear claves foráneas.
```

Doy permisos de seleccionar, insertar, actualizar y borrar sobre una base de datos al usuario escartii desde localhost.
```sql
GRANT SELECT, INSERT, UPDATE, DELETE ON database_name.* TO 'escartii'@'localhost';
```

Seleccionamos la tabla, mostramos las tablas y le damos todos los permisos en la tabla Artist.
```sql
USE Chinook;
SHOW TABLES;
ALL PRIVILEGES ON Chinook.Artist;
```

### Granularidad de permisos:
MySQL permite asignar permisos a diferentes niveles de granularidad:
- **Nivel de base de datos**.
- **Nivel de tabla**.
- **Nivel de columna**.

## Gestión de permisos y usuarios

Para gestionar usuarios y permisos, es necesario tener permisos de administrador, como **GRANT OPTION**

### Ejemplo:
```sql
GRANT ALL PRIVILEGES ON *.* TO 'admin_usuario'@'localhost';
```

## Agrupación de usuarios

La agrupación de usuarios en MySQL se realiza mediante roles a partir de la versión 8.0. Los roles permiten gestionar permisos de manera más eficiente.

### Ejemplo:
```sql
CREATE ROLE 'nombre_del_rol'; -- Si no pongo nada por defecto el host es todos.
GRANT 'nombre_del_rol' TO 'usuario1'@'localhost', 'usuario2'@'localhost';
```

## Comandos para gestionar usuarios y permisos

### Crear usuario
```sql
CREATE USER 'nuevo_usuario'@'localhost' IDENTIFIED BY 'contraseña_segura';
```

### Modificar usuario
```sql
ALTER USER 'usuario_existente'@'localhost' IDENTIFIED BY 'nueva_contraseña';
```

### Eliminar usuario
```sql
DROP USER 'usuario_a_eliminar'@'localhost';
```

### Asignar permisos
```sql
GRANT SELECT, INSERT ON base_de_datos.* TO 'usuario_existente'@'localhost';
```

### Revocar permisos
```sql
REVOKE SELECT, INSERT ON base_de_datos.* FROM 'usuario_existente'@'localhost';
```

### Crear rol
```sql
CREATE ROLE 'nombre_del_rol';
GRANT 'nombre_del_rol' TO 'usuario1'@'localhost', 'usuario2'@'localhost';
```

### Mostrar usuarios y permisos
```sql
SELECT user, host FROM mysql.user;
SHOW GRANTS FOR 'usuario_existente'@'localhost';
```

Aplicar el sol
```sql
SET ROLE 'sololectura'@'localhost'
```

Cómo saber que usuario está conectado
```sql
SELECT CURRENT_USER();
```

### Ayudas para el examen:

```sql
SELECT USER AS Rol
FROM mysql.user
WHERE HOST = '%'
    AND NOT LENGTH(authentication_string);
```