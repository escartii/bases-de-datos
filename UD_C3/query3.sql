SELECT *
-- el AS sirve para crear un alias
FROM PELICULA AS P
JOIN ACTOR AS A
ON P.CodiActor = A.CodiActor;