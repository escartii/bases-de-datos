SELECT 
    pl.PlaylistId,
    pl.Name AS PlaylistName,
    t.TrackId,
    t.Name AS TrackName,
    t.AlbumId,
    a.Title AS AlbumTitle,
    t.UnitPrice
FROM 
    Playlist pl
JOIN 
    PlaylistTrack plt ON pl.PlaylistId = plt.PlaylistId
JOIN 
    Track t ON plt.TrackId = t.TrackId
JOIN 
    Album a ON t.AlbumId = a.AlbumId
WHERE 
    pl.Name LIKE 'M%'
    AND (
        SELECT 
            COUNT(*)
        FROM 
            Track t2
        WHERE 
            t2.AlbumId = t.AlbumId
            AND t2.UnitPrice <= t.UnitPrice
    ) <= 3
ORDER BY 
    a.Title, t.UnitPrice;
