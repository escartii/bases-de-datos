SELECT COUNT(T.Name) AS "Num canciones"
FROM Track T
JOIN Album A
ON T.AlbumId = A.AlbumId
WHERE A.Title = "Out of time";